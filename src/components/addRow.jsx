import React from "react";
import { Button, Container, FormLabel, Grid, Table, TableBody, TableCell, TableContainer, TableHead, TableRow, TextField } from "@mui/material"
import { useDispatch, useSelector } from "react-redux";
import { inputTaskChangeAction, btnClickAction } from "../actions/addRow.actions";
const Task = () => {
    //Khai báo hàm sự kiện tới redux store
    const dispatch = useDispatch();
    //useSelector để đọc state từ redux
    const { inputString, taskList } = useSelector((reduxData) => {
        return reduxData.addRowReducer;
    });
    //Hàm gọi sự kiện của onChangeInputHandler
    const onChangeInputHandler = (event) => {
        dispatch(inputTaskChangeAction(event.target.value))
    };
    //Hàm gọi sự kiện khi bấm nút "Thêm"
    const btnClickHandler = () => {
        console.log('Thêm dòng');
        console.log(inputString);
        dispatch(btnClickAction());
    };
    return (
        <Container>
            {/* Là 1 hàng */}
            <Grid container spacing={2} mt={2} alignItems={"center"}>
                {/* Là 1 cột */}
                <Grid item md={3}>
                    <FormLabel>Nhập nội dung dòng</FormLabel>
                </Grid>
                <Grid item md={6}>
                    <TextField label='Nội dung' variant='outlined' fullWidth value={inputString} onChange={onChangeInputHandler} />
                </Grid>
                <Grid item md={3}>
                    <Button variant='contained' onClick={btnClickHandler}>Thêm</Button>
                </Grid>
                <TableContainer>
                    <Table>
                        <TableHead>
                            <TableRow>
                                <TableCell>STT</TableCell>
                                <TableCell>Nội dung</TableCell>
                            </TableRow>
                        </TableHead>
                        <TableBody>
                            {taskList.map((element, index) => {
                                return (
                                    <TableRow key={index}>
                                        <TableCell>
                                            {index + 1}
                                        </TableCell>
                                        <TableCell>{element.taskList}</TableCell>
                                    </TableRow>
                                )
                            })
                        }
                        </TableBody>
                    </Table>
                </TableContainer>
            </Grid>
        </Container >
    )
}

export default Task;