import { INPUT_TASK_CHANGE, BUTTON_AND_TASK_CLICKED } from "../constants/addRow.constant";

export const inputTaskChangeAction = (inputValue) => {
    return {
        type: INPUT_TASK_CHANGE,
        payload: inputValue,
    };
};

export const btnClickAction = () => {
    return {
        type: BUTTON_AND_TASK_CLICKED,
    };
};