//Root reducers
import { combineReducers } from "redux";

//Khai báo các reducer con
import addRowReducer from "./addRow.reducer";

//Tạo 01 root reducer
const rootReducer = combineReducers({
    addRowReducer
});

export default rootReducer;