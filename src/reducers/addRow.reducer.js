import { INPUT_TASK_CHANGE, BUTTON_AND_TASK_CLICKED } from "../constants/addRow.constant";

//Định nghĩa state khởi tạo cho table
const initialState = {
    inputString: "",
    tableList: []
}

//Định nghĩa khai báo table reducer
const addRowReducer = (state = initialState, action) => {
    switch (action.type) {
        case INPUT_TASK_CHANGE:
            state.inputString = action.payload; //Lấy giá trị nhập vào từ action.payload để cập nhật cho state inputString và lưu vào redux store
            break;

        case BUTTON_AND_TASK_CLICKED:
            state.taskList = [...state.taskList, state.inputString]; //để mỗi lần nhập input và nhấn button thêm sẽ cộng dồn vào mảng taskList
            state.inputString = ""; //trở về giá trị rỗng sau khi click nút thêm sẽ reset ô input
            break;

        default:
            break;
    }

    return { ...state };
}

export default addRowReducer;